package io.github.sidvenu.mathjaxtest;

import io.github.sidvenu.mathjaxview.MathJaxView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.utils.TextTool;

/**
 * demo演示页
 */
public class MainAbility extends Ability {
    private MathJaxView view;
    private String tex = "Inline formula:" +
            " $ax^2 + bx + c = 0$ " +
            "or displayed formula: $$\\sum_{i=0}^n i^2 = \\frac{(n^2+n)(2n+1)}{6}$$" +
            "Chemistry equation:$$\\ce{x Na(NH4)HPO4 ->[\\Delta] (NaPO3)_x + x NH3 ^ + x H2O}$$";

    private TextField tfOriTxt;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        view = (MathJaxView) findComponentById(ResourceTable.Id_formula2);

        tfOriTxt = (TextField) findComponentById(ResourceTable.Id_tf_ori_txt);

        tfOriTxt.setText(tex);

        findComponentById(ResourceTable.Id_btn).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!TextTool.isNullOrEmpty(tfOriTxt.getText())) {
                    view.setText(tfOriTxt.getText());
                }
                tfOriTxt.setText("");
            }
        });
    }
}
