# MathJaxView

## 项目介绍
- 项目名称：MathJaxView
- 所属系列：openharmony的第三方组件适配移植
- 功能：自定义webview离线加载格式化数学公式
- 项目移植状态：主功能完成
- 调用差异：公式的字符字体有差异
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.0.7

## 效果演示
![screen1](  /screenshots/mathjaxview.gif "屏幕截图.gif")



## 安装教程


1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
 dependencies {
 implementation('com.gitee.chinasoft_ohos:MathJaxView:1.0.0');
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
 XML

```
 <io.github.sidvenu.mathjaxview.MathJaxView
         ohos:id="$+id:formula1"
         ohos:height="60vp"
         ohos:width="match_parent"
         pc:text="$$2^x=15$$"/>
```
 Programatically
```
    MathJaxView view = (MathJaxView) findComponentById(ResourceTable.Id_formula2);
    view.setText("Inline formula: $ax^2 + bx + c = 0$ or displayed formula: $$\\sum_{i=0}^n i^2 = \\frac{(n^2+n)(2n+1)}{6}$$ Chemistry equation:$$\\ce{x Na(NH4)HPO4 ->[\\Delta] (NaPO3)_x + x NH3 ^ + x H2O}$$");

```


## 测试信息

- CodeCheck代码测试无异常

- CloudTest代码测试无异常

- 病毒安全检测通过

- 当前版本demo功能与原组件基本无差异

## 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT

## 版权和许可信息

- Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
  
      http://www.apache.org/licenses/LICENSE-2.0
  
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.

